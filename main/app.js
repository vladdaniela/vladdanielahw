function distance(first, second){
    let a=false
    
    if ((toString.call(first) === "[object Array]")&&(toString.call(second)==="[object Array]"))
    {
        a=true
    }
    if(a===true)    
    {
        let f=[...new Set(first)]
        let s=[...new Set(second)]
        let distance=0
        if(Array.isArray(f)&&f.length&&Array.isArray(s)&&s.length){
            if(f.length>s.length)
            {
                for(let i=0;i<f.length;i++){
                    if(s.indexOf(f[i])===-1){
                        distance++
                    }
                }
            }
            else
            {
                if(f.length<s.length){
                    for(let i=0;i<s.length;i++){
                        if(f.indexOf(s[i])===-1){
                            distance++
                        }
                    }
                }
            }
            return distance
        }
        else{
            return 0
        }
    } 
    else{
        throw new Error('InvalidType');
        
    }  
}


// let a=[2,3,"ana", "ana", "dana", "mimi", "cici"]
// let b=["dana", 2,3, "ioana"]

// console.log(distance(a,b))




module.exports.distance = distance